all: .env install dev

up:
	docker-compose up -d

install: up
	docker-compose exec node bash -c "npm install"

dev: up
	docker-compose exec node bash -c "npm run dev"

build: up
	docker-compose exec node bash -c "npm run build"

.env: up
	docker-compose exec node bash -c "cp -n .env.dist .env"

bash: up
	docker-compose exec node bash

publish: up
	docker-compose exec node bash -c "./publish.sh"
	#
	# Published to: public/index.html
	#
