Pixi.js DragonBonesJS seed
==========================

Skeleton for a pixi.js DragonBonesJS application.

Demo: <https://alcalyn.gitlab.io/pixi-dragonbones-skeleton>.

It contains:

- NodeJS, NPM and Typescript environment to develop your pixi.js/DragonBonesJS application
- Docker environment
- Gitlab CI deployment
- Demo application (walking girl)
- Webpack watch files
- Webpack lint Typescript files
- Webpack publish production files


## Requirements

For Development:

- Either **NodeJS and NPM**, or **Docker and docker-compose**.

For Design and animation:

- **Blender** with **COA tools plugin** to edit animations.
- To draw animated personnages, you can use **Krita** or **Gimp**, both can export sprites to Blender + COA tools.


## Install

When you just cloned the repo:

``` bash
# Create .env file
cp .env.dist .env

# Install NPM dependencies
npm install
```

Or using Docker:

``` bash
make install
```


## Development

When you want to edit Typescript files in `src/`:

``` bash
# Run webpack watch and start server
npm run dev
```

Using Docker:

``` bash
make dev
```


## Publish

When you want to compile the application, and dump minified assets:

``` bash
./publish.sh
```

Using Docker:

``` bash
make publish
```

It will dump application in `public/` folder.

It can be used to publish to production.
Also, this is the same script used for publish with Gitlab CI.


## References

- [Pixi.js](http://www.pixijs.com/)
- [Pixi.js jsdoc](http://pixijs.download/release/docs/index.html)
- [Pixi.js examples](https://pixijs.io/examples/#/basics/basic.js)
- [DragonBonesJS/Pixi repo](https://github.com/DragonBones/DragonBonesJS/tree/master/Pixi)
- [DragonBonesJS/Pixi examples](https://github.com/DragonBones/DragonBonesJS/tree/master/Pixi/Demos/src)
- [COA tools plugin for Blender](https://github.com/ndee85/coa_tools)


## License

This library is under [MIT License](LICENSE).
